import React, { useState } from 'react'
import { Estafeta, IData, Sap, Shopify, WhatsApp } from './IData'
import {
  initialEstafeta,
  initialSap,
  initialShopify,
  initialState,
  initialWhatsApp
} from './initialState'
import { postData } from './axios'

const Form = () => {
  const [data, setData] = useState<IData>(initialState)
  const [sap, setSap] = useState<Sap>(initialSap)
  const [estafeta, setEstafeta] = useState<Estafeta>(initialEstafeta)
  const [shopify, setShopify] = useState<Shopify>(initialShopify)
  const [whatsApp, setWhatsApp] = useState<WhatsApp>(initialWhatsApp)

  const handleChange = (e: ChangeEvent<HtmlInputElement>, setElement: Function) =>
    setElement({ ...data, [e.target.name]: e.target.value })

  const handleSubmit = async (e: SubmitEvent<HTMLFormElement>) => {
    e.preventDefault()

    const objetoFinal: IData = {
      ...data,
      estafeta,
      sap,
      shopify,
      whatsApp
    }

    const result = await postData(objetoFinal)
    alert(result)
  }

  return (
    <div>
      <h1>Form</h1>
      <form onSubmit={handleSubmit}>
        {/* Agregar todos los inputs para data */}
        <input
          type='text'
          name='name'
          onChange={(e) => handleChange(e, setData)}
          value={data.name}
        />

        <input
          type='file'
          name='image'
          onChange={(e) => handleChange(e, setData)}
          value={data.image}
        />

        {/* Agregar todos los inputs para sap */}
        <input
          type='text'
          name='division'
          onChange={(e) => handleChange(e, setSap)}
          value={sap.division}
        />

        <input
          type='text'
          name='distributionChannel'
          onChange={(e) => handleChange(e, setSap)}
          value={sap.distributionChannel}
        />

        {/* Agregar todos los inputs para estafeta */}
        <input
          type='text'
          name='applicationId'
          onChange={(e) => handleChange(e, setEstafeta)}
          value={estafeta.applicationId}
        />

        <input
          type='text'
          name='customerNumber'
          onChange={(e) => handleChange(e, setEstafeta)}
          value={estafeta.customerNumber}
        />

        {/* Agregar todos los inputs para shopify */}
        <input
          type='text'
          name='apiVersion'
          onChange={(e) => handleChange(e, setShopify)}
          value={shopify.apiVersion}
        />

        <input
          type='text'
          name='endpoint'
          onChange={(e) => handleChange(e, setShopify)}
          value={shopify.endpoint}
        />

        {/* Agregar todos los inputs para whastapp */}
        <input
          type='text'
          name='defaultDelivery'
          onChange={(e) => handleChange(e, setWhatsApp)}
          value={whatsApp.defaultDelivery}
        />

        <input
          type='text'
          name='orderCreatedTemplateName'
          onChange={(e) => handleChange(e, setWhatsApp)}
          value={whatsApp.orderCreatedTemplateName}
        />
      </form>
    </div>
  )
}

export default Form
