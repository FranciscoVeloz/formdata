import axios from 'axios'
import FormData from 'form-data'
import { IData } from './IData'

export const postData = async (data: IData) => {
  const formData = new FormData()

  //Todos los elementos del objeto data los metemos al formData
  Object.entries(data).map(([key, value]) => {
    formData.append(key, value)
  })

  const respuesta = await axios.post('/url', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })

  return respuesta.data
}
