export interface Estafeta {
  apikey: string;
  applicationId: string;
  customerNumber: string;
  name: string;
  outputType: string;
  printingTemplate: string;
  sharedSecret: string;
  subscriberId: string;
  version: string;
}

export interface Sap {
  distributionChannel: string;
  division: string;
  pricelist: string;
  salesOrganization: string;
  scheduleline: string;
  shippingType: string;
}

export interface Shopify {
  accessToken: string;
  apiVersion: string;
  endpoint: string;
  hashSign: string;
  jobOrderRecurringMinutes: number;
  secretKey: string;
}

export interface WhatsApp {
  accountId: string;
  defaultDelivery: string;
  orderCreatedTemplateName: string;
  orderFulfilledTemplateName: string;
  permanentToken: string;
}

export interface IData {
  estafeta: Estafeta;
  id: string;
  image: string;
  name: string;
  sap: Sap;
  shopify: Shopify;
  WhatsApp: WhatsApp;
}
