import { Estafeta, IData, Sap, Shopify, WhatsApp } from "./IData";

export const initialEstafeta: Estafeta = {
  apikey: "",
  applicationId: "",
  customerNumber: "",
  name: "",
  outputType: "",
  printingTemplate: "",
  sharedSecret: "",
  subscriberId: "",
  version: "",
};

export const initialSap: Sap = {
  distributionChannel: "",
  division: "",
  pricelist: "",
  salesOrganization: "",
  scheduleline: "",
  shippingType: "",
};

export const initialShopify: Shopify = {
  accessToken: "",
  apiVersion: "",
  endpoint: "",
  hashSign: "",
  jobOrderRecurringMinutes: 0,
  secretKey: "",
};

export const initialWhatsApp: WhatsApp = {
  accountId: "",
  defaultDelivery: "",
  orderCreatedTemplateName: "",
  orderFulfilledTemplateName: "",
  permanentToken: "",
};

export const initialState: IData = {
  estafeta: initialEstafeta,
  id: "",
  image: "",
  name: "",
  sap: initialSap,
  shopify: initialShopify,
  WhatsApp: initialWhatsApp,
};
